package com.nurstore.db.dao;

import com.nurstore.model.Cart;

import java.io.IOException;

public interface CartDao {

    Cart getCartById(Long id);

    void updateCart(Cart cart);

    Cart validate(Long cartId) throws IOException;

}
