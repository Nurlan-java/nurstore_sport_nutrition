package com.nurstore.db.dao;

import com.nurstore.model.Cart;
import com.nurstore.model.CartItem;

public interface CartItemDao {


    void addCartItem(CartItem cartItem);

    void removeCartItem(CartItem cartItem);

    void removeAllCartItems(Cart cart);

    CartItem getCartItemByProductId(Long productId);

}
