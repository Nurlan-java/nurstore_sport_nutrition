package com.nurstore.db.dao;

import com.nurstore.model.Customer;

import java.util.List;

public interface CustomerDao {

    void addCustomer(Customer customer);

    Customer getCustomerById(Long id);

    List<Customer> getAllCustomers();

    Customer getCustomerByUsername(String username);

}
