package com.nurstore.db.dao;

import com.nurstore.model.Order;

public interface OrderDao {

    void addOrder(Order order);
    double getOrderGrandTotal(Long cartId);
}
