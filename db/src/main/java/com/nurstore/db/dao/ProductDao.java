package com.nurstore.db.dao;

import com.nurstore.model.Product;

import java.util.List;


public interface ProductDao {

    void addProduct(Product product);

    void editProduct(Product product);

    Product getProductById(Long productId) throws Exception;

    List<Product> getAllProducts();

    void deleteProduct(Long productId);


}
