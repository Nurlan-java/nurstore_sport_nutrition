package com.nurstore.db.dao.impl;

import com.nurstore.db.dao.CartDao;
import com.nurstore.db.dao.OrderDao;
import com.nurstore.model.Cart;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;

@Repository
@Transactional
public class CartDaoImpl implements CartDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private OrderDao orderDao;

    @Override
    public Cart getCartById(Long id) {

        Session session = sessionFactory.getCurrentSession();
        return session.get(Cart.class, id);
    }

    @Override
    public void updateCart(Cart cart) {
        long cartId = cart.getCartId();
        double grandTotal = orderDao.getOrderGrandTotal(cartId);
        cart.setGrandTotal(grandTotal);

        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(cart);

    }

    @Override
    public Cart validate(Long cartId) throws IOException {

        Cart cart = getCartById(cartId);

        if (cart == null || cart.getCartItems().size() == 0) {
            throw new IOException(cartId + "");
        }

        updateCart(cart);

        return cart;
    }
}
