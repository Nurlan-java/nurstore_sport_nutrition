package com.nurstore.db.dao.impl;

import com.nurstore.db.dao.CartItemDao;
import com.nurstore.model.Cart;
import com.nurstore.model.CartItem;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class CartItemDaoImpl implements CartItemDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addCartItem(CartItem cartItem) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(cartItem);
        session.flush();
    }

    @Override
    public void removeCartItem(CartItem cartItem) {

        Session session = sessionFactory.getCurrentSession();
        session.delete(cartItem);
        session.flush();

    }

    @Override
    public void removeAllCartItems(Cart cart) {

        List<CartItem> cartItems = cart.getCartItems();

        for (CartItem item : cartItems) {
            removeCartItem(item);
        }
    }

    @Override
    public CartItem getCartItemByProductId(Long productId) {

        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM CartItem as i  WHERE i.product.productId = :productId");
        query.setParameter("productId", productId);
        session.flush();

        return (CartItem) query.uniqueResult();
    }
}
