package com.nurstore.db.dao.impl;

import com.nurstore.db.dao.CustomerDao;
import com.nurstore.model.Authorities;
import com.nurstore.model.Cart;
import com.nurstore.model.Customer;
import com.nurstore.model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class CustomerDaoImpl implements CustomerDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addCustomer(Customer customer) {

        Session session = sessionFactory.getCurrentSession();

        customer.getBillingAddress().setCustomer(customer);
        customer.getShippingAddress().setCustomer(customer);

        session.saveOrUpdate(customer);
        session.saveOrUpdate(customer.getBillingAddress());
        session.saveOrUpdate(customer.getShippingAddress());

        User newUser = new User();

        newUser.setUsername(customer.getUsername());
        newUser.setPassword(customer.getPassword());
        newUser.setEnabled(true);
        newUser.setCustomerId(customer.getCustomerId());

        Authorities newAuthority = new Authorities();

        newAuthority.setUsername(customer.getUsername());
        newAuthority.setAuthority("ROLE_USER");

        session.saveOrUpdate(newUser);
        session.saveOrUpdate(newAuthority);

        Cart newCart = new Cart();
        newCart.setCustomer(customer);
        customer.setCart(newCart);

        session.saveOrUpdate(customer);
        session.saveOrUpdate(newCart);

        session.flush();

    }

    @Override
    public Customer getCustomerById(Long id) {

        Session session = sessionFactory.getCurrentSession();
        return session.get(Customer.class, id);
    }

    @Override
    public List<Customer> getAllCustomers() {

        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Customer ");

        List<Customer> customers = query.list();

        return customers;
    }

    @Override
    public Customer getCustomerByUsername(String username) {

        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Customer as c  WHERE c.username = :username");
        query.setParameter("username", username);
        return (Customer) query.uniqueResult();
    }
}
