package com.nurstore.db.dao.impl;

import com.nurstore.db.dao.CartDao;
import com.nurstore.db.dao.OrderDao;
import com.nurstore.model.Cart;
import com.nurstore.model.CartItem;
import com.nurstore.model.Order;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class OrderDaoImpl implements OrderDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private CartDao cartDao;

    @Override
    public void addOrder(Order order) {

        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(order);
        session.flush();

    }

    @Override
    public double getOrderGrandTotal(Long cartId) {

        double grandTotal = 0;
        Cart cart = cartDao.getCartById(cartId);
        List<CartItem> cartItems = cart.getCartItems();

        for (CartItem cartItem : cartItems) {
            grandTotal += cartItem.getTotalPrice();
        }
        return grandTotal;
    }
}
