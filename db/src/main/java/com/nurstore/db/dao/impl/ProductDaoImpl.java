package com.nurstore.db.dao.impl;

import com.nurstore.db.dao.ProductDao;
import com.nurstore.model.Product;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class ProductDaoImpl implements ProductDao {


    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addProduct(Product product) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(product);
        session.flush();
    }


    @Override
    public void editProduct(Product product) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(product);
        session.flush();
    }

    @Override
    public Product getProductById(Long productId) {
        Session session = sessionFactory.getCurrentSession();

        return session.get(Product.class, productId);

    }

    @Override
    public List<Product> getAllProducts() {
        Session session = sessionFactory.openSession();
        final Query<Product> query = session.createNativeQuery("SELECT * FROM products;", Product.class);
        return query.getResultList();

    }

    @Override
    public void deleteProduct(Long productId) {
        Session session = sessionFactory.getCurrentSession();
        session.remove(getProductById(productId));
        session.flush();
    }
}
