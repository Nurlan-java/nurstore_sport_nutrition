CREATE USER 'nurstore_sport_nutrition'@'localhost' IDENTIFIED BY 'nurstore_sport_nutrition';
GRANT ALL PRIVILEGES ON *.* TO 'nurstore_sport_nutrition'@'localhost';

CREATE DATABASE IF NOT EXISTS `nurstore_sport_nutrition` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `nurstore_sport_nutrition`;



CREATE TABLE users
(
    `id`       BIGINT      NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(50) NOT NULL,
    `password` VARCHAR(50) NOT NULL,
    `enabled`  BOOLEAN,
    PRIMARY KEY (`id`)
);

CREATE TABLE authorities
(
    `id`        BIGINT      NOT NULL AUTO_INCREMENT,
    `username`  VARCHAR(50) NOT NULL,
    `authority` VARCHAR(50) NOT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT fk_authorities_users FOREIGN KEY (id) REFERENCES users (id)
);

INSERT INTO `nurstore_sport_nutrition`.`users` (`username`, `password`, `enabled`)
VALUES ('admin', 'admin@1234', true);

INSERT INTO `nurstore_sport_nutrition`.`authorities` (`username`, `authority`)
VALUES ('admin', 'ROLE_USER');