CREATE TABLE `nurstore_sport_nutrition`.`product`
(
    `id`                BIGINT        NOT NULL AUTO_INCREMENT,
    `name`              VARCHAR(45)   NULL,
    `category`          VARCHAR(45)   NULL,
    `description`       VARCHAR(500)  NULL,
    `price`             DECIMAL(5, 2) NULL,
    `product_condition` VARCHAR(45)   NULL,
    `status`            VARCHAR(45)   NULL,
    `unit_in_stock`     INT           NULL,
    `manufacturer`      VARCHAR(45)   NULL,
    PRIMARY KEY (`id`)
);



INSERT INTO `nurstore_sport_nutrition`.`product` (`id`,
                                                  `name`, `category`, `description`, `price`, `product_condition`,
                                                  `status`, `unit_in_stock`, `manufacturer`)
VALUES ('1', 'Nike Huarache Khaki', 'Men', 'The sneakers first landed back in 1991
  in the form of the Nike Air Huarache, that runner that was one of the earliest examples
  of Nike willfully creating a sneaker that was “stripped to the bare essentials”.',
        '180', 'New', 'Active', '15', 'Nike');
