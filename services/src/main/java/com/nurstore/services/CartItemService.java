package com.nurstore.services;

import com.nurstore.model.Cart;
import com.nurstore.model.CartItem;

public interface CartItemService {

    void addCartItem(CartItem cartItem);

    void removeCartItem(CartItem cartItem);

    void removeAllCartItems(Cart cart);

    CartItem getCartItemByProductId(Long productId);


}
