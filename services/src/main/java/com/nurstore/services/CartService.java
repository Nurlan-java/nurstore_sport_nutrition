package com.nurstore.services;

import com.nurstore.model.Cart;

public interface CartService {

    Cart getCartById(Long id);
    void updateCart(Cart cart);
}
