package com.nurstore.services;

import com.nurstore.model.Order;

public interface OrderService {

    void addOrder(Order order);

    double getOrderGrandTotal(Long cartId);
}
