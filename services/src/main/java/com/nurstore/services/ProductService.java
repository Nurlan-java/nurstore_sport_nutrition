package com.nurstore.services;

import com.nurstore.model.Product;

import java.util.List;

public interface ProductService {

    void addProduct(Product product);

    void editProduct(Product product);

    Product getProductById(Long productId) throws Exception;

    List<Product> getAllProducts();

    void deleteProduct(Long productId);

}
