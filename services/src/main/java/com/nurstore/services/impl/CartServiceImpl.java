package com.nurstore.services.impl;

import com.nurstore.db.dao.CartDao;
import com.nurstore.model.Cart;
import com.nurstore.services.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CartServiceImpl implements CartService {

    @Autowired
    private CartDao cartDao;


    @Override
    public Cart getCartById(Long id) {
        return cartDao.getCartById(id);
    }

    @Override
    public void updateCart(Cart cart) {
        cartDao.updateCart(cart);
    }
}
