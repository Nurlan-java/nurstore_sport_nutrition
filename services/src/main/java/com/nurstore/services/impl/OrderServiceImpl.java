package com.nurstore.services.impl;

import com.nurstore.db.dao.OrderDao;
import com.nurstore.model.Order;
import com.nurstore.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderDao orderDao;

    @Override
    public void addOrder(Order order) {

        orderDao.addOrder(order);

    }

    @Override
    public double getOrderGrandTotal(Long cartId) {
        return orderDao.getOrderGrandTotal(cartId);
    }
}
