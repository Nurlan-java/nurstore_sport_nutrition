package com.nurstore.services.impl;

import com.nurstore.services.ProductService;
import com.nurstore.db.dao.ProductDao;
import com.nurstore.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductDao productDao;

    @Override
    public void addProduct(Product product) {
        productDao.addProduct(product);
    }

    @Override
    public void editProduct(Product product) {
        productDao.editProduct(product);

    }

    @Override
    public Product getProductById(Long productId) throws Exception {
        return productDao.getProductById(productId);
    }

    @Override
    public List<Product> getAllProducts() {
        return productDao.getAllProducts();
    }

    @Override
    public void deleteProduct(Long productId) {
        productDao.deleteProduct(productId);
    }
}
