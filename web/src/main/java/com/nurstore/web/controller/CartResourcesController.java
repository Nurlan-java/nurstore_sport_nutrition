package com.nurstore.web.controller;

import com.nurstore.model.Cart;
import com.nurstore.model.CartItem;
import com.nurstore.model.Customer;
import com.nurstore.model.Product;
import com.nurstore.services.CartItemService;
import com.nurstore.services.CartService;
import com.nurstore.services.CustomerService;
import com.nurstore.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cart")
public class CartResourcesController {

    @Autowired
    private CartService cartService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private ProductService productService;

    @Autowired
    private CartItemService cartItemService;

    @RequestMapping("/{cartId}")
    public @ResponseBody
    Cart getCartById(@PathVariable Long cartId) {
        return cartService.getCartById(cartId);
    }

    @PutMapping("/add/{productId}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void addItem(@PathVariable Long productId,
                        @AuthenticationPrincipal User activeUser) throws Exception {

        Customer customer =
                customerService.getCustomerByUsername(activeUser.getUsername());

        Cart cart = customer.getCart();

        Product product = productService.getProductById(productId);
        List<CartItem> cartItems = cart.getCartItems();

        for (CartItem item : cartItems) {
            if (product.getProductId().equals(item.getProduct().getProductId())) {
                CartItem cartItem = item;
                cartItem.setQuantity(cartItem.getQuantity() + 1);
                cartItem.setTotalPrice(product.getPrice() * cartItem.getQuantity());
                cartItemService.addCartItem(cartItem);
                return;
            }
        }

        CartItem cartItem = new CartItem();
        cartItem.setProduct(product);
        cartItem.setQuantity(1);
        cartItem.setTotalPrice(product.getPrice() * cartItem.getQuantity());
        cartItem.setCart(cart);
        cartItemService.addCartItem(cartItem);

    }

    @PutMapping("/remove/{productId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeItem(@PathVariable Long productId) {
        CartItem cartItem = cartItemService.getCartItemByProductId(productId);
        cartItemService.removeCartItem(cartItem);
    }

    @DeleteMapping("/{cartId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void clearCart(@PathVariable Long cartId) {
        Cart cart = cartService.getCartById(cartId);
        cartItemService.removeAllCartItems(cart);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST,
            reason = "Illegal request, please verify your payload.")
    public void handleClientErrors(Exception e) {
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR,
            reason = "Internal Server error.")
    public void handleServerErrors(Exception e) {
    }

}
