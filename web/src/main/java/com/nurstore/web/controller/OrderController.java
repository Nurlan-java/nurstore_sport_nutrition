package com.nurstore.web.controller;

import com.nurstore.model.Cart;
import com.nurstore.model.Customer;
import com.nurstore.model.Order;
import com.nurstore.services.CartService;
import com.nurstore.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class OrderController {

    @Autowired
    private CartService cartService;

    @Autowired
    private OrderService orderService;

    @RequestMapping("/order/{cartId}")
    public String createOrder(@PathVariable Long cartId) {
        Order order = new Order();
        Cart cart = cartService.getCartById(cartId);
        order.setCart(cart);

        Customer customer = cart.getCustomer();

        order.setCustomer(customer);
        order.setBillingAddress(customer.getBillingAddress());
        order.setShippingAddress(customer.getShippingAddress());

        orderService.addOrder(order);

        return "redirect:/checkout?cartId="+cartId;

    }

}
