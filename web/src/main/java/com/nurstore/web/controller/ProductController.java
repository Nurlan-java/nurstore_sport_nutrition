package com.nurstore.web.controller;

import com.nurstore.model.Product;
import com.nurstore.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @RequestMapping("/productList/all")
    public String getProducts(Model model) {
        List<Product> products = productService.getAllProducts();
        model.addAttribute("products", products);
        return "productList";
    }

    @RequestMapping("/viewProduct/{productId}")
    public String viewProduct(@PathVariable Long productId, Model model) {

        try {
            Product product = productService.getProductById(productId);
            model.addAttribute("product", product);
        } catch (Exception e) {
            throw new RuntimeException("Product with ID:" + productId + "not found");
        }

        return "viewProduct";
    }

    @RequestMapping("/productList")
    public String getProductByCategory(@RequestParam(required = false, name = "searchCondition")
                                               String searchCondition, Model model) {

        List<Product> products = productService.getAllProducts();
        model.addAttribute("products", products);
        model.addAttribute("searchCondition", searchCondition);

        return "productList";


    }
}
