package com.nurstore.web.controller;

import com.nurstore.model.BillingAddress;
import com.nurstore.model.Customer;
import com.nurstore.model.ShippingAddress;
import com.nurstore.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class RegistrationController {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private CustomerService customerService;

    @GetMapping("/register")
    public String registerCustomer(Model model) {

        Customer customer = new Customer();
        BillingAddress billingAddress = new BillingAddress();
        ShippingAddress shippingAddress = new ShippingAddress();
        customer.setBillingAddress(billingAddress);
        customer.setShippingAddress(shippingAddress);

        model.addAttribute("customer", customer);

        return "registerCustomer";
    }

    @PostMapping("/register")
    public String registerCustomerPost(@Valid @ModelAttribute Customer customer,
                                       BindingResult result, Model model) {

        if (result.hasErrors()) {
            return "registerCustomer";
        }

        List<Customer> customers = customerService.getAllCustomers();

        for (Customer existingCustomer : customers) {
            if (customer.getUsername().equals(existingCustomer.getUsername())) {
                model.addAttribute("usernameMsg", "Username already exists");
                return "registerCustomer";
            }
        }

        for (Customer existingCustomer : customers) {
            if (customer.getEmail().equals(existingCustomer.getEmail())) {
                model.addAttribute("emailMsg", "Email already exists");
                return "registerCustomer";
            }
        }

        customer.setPassword(bCryptPasswordEncoder.encode(customer.getPassword()));
        customer.setEnabled(true);

        customerService.addCustomer(customer);

        return "registerCustomerSuccess";
    }
}
