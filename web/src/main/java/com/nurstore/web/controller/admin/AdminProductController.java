package com.nurstore.web.controller.admin;

import com.nurstore.model.Product;
import com.nurstore.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Controller
@MultipartConfig
@RequestMapping("/admin")
public class AdminProductController {

    private Path path;

    @Autowired
    private ProductService productService;

    @GetMapping("/product/addProduct")
    public String addProduct(Model model) {
        Product product = new Product();
        product.setCategory("men");
        product.setStatus("active");

        model.addAttribute("product", product);

        return "addProduct";
    }

    @PostMapping("/product/addProduct")
    public String addProduct(@Valid @ModelAttribute("product") Product product,
                             BindingResult result,
                             HttpServletRequest request) {

        if (result.hasErrors()) {
            return "addProduct";
        }

        addProductImage(product, request);

        productService.addProduct(product);

        return "redirect:/admin/productInventory";
    }

    @GetMapping("/product/editProduct/{productId}")
    public String editProduct(@PathVariable Long productId, Model model) throws Exception {

        Product product = productService.getProductById(productId);

        model.addAttribute("product", product);

        return "editProduct";
    }

    @PostMapping("/product/editProduct")
    public String editProduct(@Valid @ModelAttribute("product") Product product,
                              BindingResult result,
                              HttpServletRequest request) {

        if (result.hasErrors()) {
            return "editProduct";
        }

        addProductImage(product, request);

        productService.editProduct(product);

        return "redirect:/admin/productInventory";
    }

    @RequestMapping("/product/deleteProduct/{productId}")
    public String deleteProduct(@PathVariable Long productId, HttpServletRequest request) {

        String rootDirectory =
                request.getSession().getServletContext().getRealPath("/");

        path = Paths.get(rootDirectory + "\\WEB-INF\\resources\\images" + productId + ".png");

        if (Files.exists(path)) {
            try {
                Files.delete(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        productService.deleteProduct(productId);

        return "redirect:/admin/productInventory";

    }

    private void addProductImage(@ModelAttribute("product") @Valid Product product, HttpServletRequest request) {
        MultipartFile productImage = product.getImage();
        String rootDirectory =
                request.getSession().getServletContext().getRealPath("/");

        path = Paths.get(rootDirectory + "\\WEB-INF\\resources\\images" + product.getProductId() + ".png");

        if (productImage != null && !productImage.isEmpty()) {
            try {
                productImage.transferTo(new File(path.toString()));
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException("Product image saving failed.", e);
            }
        }
    }
}
