<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ include file="../../view/templates/header.jsp" %>


<div class="container-wrapper">
    <div class="container">
        <div class="page-header">
            <h1 align="center">Customer</h1>

            <p class="lead">Customer Details:</p>
        </div>

        <form:form class="form-horizontal" modelAttribute="order">

        <h3>Customer Info</h3>

        <div class="form-group">
            <label for="name">Name</label>
            <form:input path="cart.customer.name" id="name" class="form-Control"/>
        </div>

        <div class="form-group">
            <label for="email">Email</label>
            <form:input path="cart.customer.email" id="email" class="form-Control"/>
        </div>

        <div class="form-group">
            <label for="phone">Phone</label>
            <form:input path="cart.customer.phone" id="phone" class="form-Control"/>
        </div>

        <h3>Billing Address</h3>

        <div class="form-group">
            <label for="billingStreet">Street Name</label>
            <form:input path="cart.customer.billingAddress.street" id="billingStreet" class="form-Control"/>
        </div>

        <div class="form-group">
            <label for="billingApartmentNumber">Apartment Number</label>
            <form:input path="cart.customer.billingAddress.apartmentNumber" id="billingApartmentNumber"
                        class="form-Control"/>
        </div>

        <div class="form-group">
            <label for="billingCity">City</label>
            <form:input path="cart.customer.billingAddress.city" id="billingCity" class="form-Control"/>
        </div>

        <div class="form-group">
            <label for="billingCountry">Country</label>
            <form:input path="cart.customer.billingAddress.country" id="billingCountry" class="form-Control"/>
        </div>

        <div class="form-group">
            <label for="billingZip">Zipcode</label>
            <form:input path="cart.customer.billingAddress.zipCode" id="billingZip" class="form-Control"/>
        </div>

        <input type="hidden" name="_flowExecutionKey"/>

        <br><br>
        <input type="submit" value="Next" class="btn btn-lg btn-primary" name="_eventId_customerInfoCollected"/>
        <button class="btn btn-primary" name="_eventId_cancel">Cancel</button>
        </form:form>

        <%@ include file="../../view/templates/footer.jsp" %>

