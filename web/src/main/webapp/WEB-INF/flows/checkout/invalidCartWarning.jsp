<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ include file="../../view/templates/header.jsp" %>
<div class="container-wrapper">
    <div class="container">
        <section>
            <div class="jumbotron">
                <div class="container">
                    <h1>Invalid Cart</h1>
                </div>
            </div>
        </section>

        <section class="container">
            <p>
                <a href="<spring:url value="/product/productList" />" class="btn btn-lg btn-primary">Products</a>
            </p>
        </section>

    </div>
</div>

<%@ include file="../../view/templates/footer.jsp" %>
