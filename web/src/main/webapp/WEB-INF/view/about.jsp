<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ include file="templates/header.jsp" %>
<div class="container-wrapper">
    <div class="container">
        <div class="page-header">
            <h1 align="center">About Nurstore</h1>
            <p>
                We are a humble team who are passionate
                about helping our customers
                to look more fit and healthy
                so they can enjoy their lives to the fullest.
                We are determined to provide the best products and services.
            </p>
        </div>

        <img src="<c:url value="/resources/images/showRoom.jpg"/>"
             alt="Show Room" width="1000" height="500">

    </div>
</div>

<%@ include file="templates/footer.jsp" %>
