<%@ include file="templates/header.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<div class="container-wrapper">
    <div class="container">
        <div class="page-header">
            <h1 align="center">Administrator Page</h1>
        </div>

        <br>

        <c:if test="${pageContext.request.userPrincipal.name != null}">
        <h2>
            Welcome: ${pageContext.request.userPrincipal.name} | <a href="<c:url value="/logout"/>"/>Logout</a>
        </h2>
        </c:if>

        <br><br>

        <h3>
            <a href="<c:url value="/admin/productInventory" />"> Product Inventory </a>
        </h3>

        <p> Here you can view, check and modify the product inventory </p>

        <br><br>

        <h3>
            <a href="<c:url value="/admin/customer" />"> Customer Management </a>
        </h3>

        <p> Here you can view information about customers </p>


        <%@ include file="templates/footer.jsp" %>
