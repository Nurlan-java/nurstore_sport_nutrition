<%@ taglib prefix="spring" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="templates/header.jsp" %>

<div class="container-wrapper">
    <div class="container">
        <div class="page-header">
            <h1>Customer Management Page</h1>

            <p class="lead">Here you can view, update and delete customers</p>
        </div>

        <table class="table table-striped table-hover">

            <thead>
            <tr class="bg-success">
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Username</th>
                <th>Enabled</th>
            </tr>
            </thead>

            <c:forEach items="${customers}" var="customer">

                <tr>
                    <td>${customer.name}</td>
                    <td>${customer.email}</td>
                    <td>${customer.phone}</td>
                    <td>${customer.username}</td>
                    <td>${customer.enabled}</td>
                </tr>

            </c:forEach>

        </table>

        <%@ include file="templates/footer.jsp" %>
