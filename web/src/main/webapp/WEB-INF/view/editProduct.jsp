<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ include file="templates/header.jsp" %>


<div class="container-wrapper">
    <div class="container">
        <div class="page-header">
            <h1>Edit Product </h1>

            <p class="lead">Please update the product information: </p>
        </div>

        <form:form action="${pageContext.request.contextPath}/admin/product/editProduct/"
                   method="post" modelAttribute="product" enctype="multipart/form-data">
            <form:hidden path="productId" value="${product.productId}"/>

        <div class="form-group">
            <label for="name">Name</label>
            <form:input path="name" id="name" class="form-Control" value="${product.name}"/>
        </div>

        <div class="form-group">
            <label for="category">Category</label>
            <label class="checkbox-inline"><form:radiobutton path="category" id="category"
                                                             value="men"/>Men</label>
            <label class="checkbox-inline"><form:radiobutton path="category" id="category"
                                                             value="women"/>Women</label>
            <label class="checkbox-inline"><form:radiobutton path="category" id="category"
                                                             value="teenagers"/>Teenagers</label>
        </div>

        <div class="form-group">
            <label for="description">Description</label>
            <form:textarea path="description" id="description" class="form-Control" value="${product.description}"/>
        </div>

        <div class="form-group">
            <label for="price">Price</label>
            <form:input path="price" id="price" class="form-Control" value="${product.price}"/>
        </div>

        <div class="form-group">
            <label for="status">Status</label>
            <label class="checkbox-inline"><form:radiobutton path="status" id="status"
                                                             value="active"/>Active</label>
            <label class="checkbox-inline"><form:radiobutton path="status" id="status"
                                                             value="inactive"/>Inactive</label>
        </div>

        <div class="form-group">
            <label for="unitInStock">Unit in stock</label>
            <form:input path="unitInStock" id="unitInStock" class="form-Control" value="${product.unitInStock}"/>
        </div>

        <div class="form-group">
            <label for="manufacturer">Manufacturer</label>
            <form:input path="manufacturer" id="manufacturer" class="form-Control" value="${product.manufacturer}"/>
        </div>

        <div class="form-group">
            <label class="control-label" for="image">Upload Picture</label>
            <form:input id="image" path="image" type="file" class="form:input-large"/>
        </div>

        <br><br>

        <input type="submit" value="Submit" class="btn btn-default">
        <a href="<c:url value="/admin/productInventory"/> " class="btn btn-default">Cancel</a>

        </form:form>


        <%@ include file="templates/footer.jsp" %>
