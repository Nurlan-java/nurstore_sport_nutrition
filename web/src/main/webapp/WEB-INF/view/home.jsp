<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ include file="templates/header.jsp" %>


<main role="main">

    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="first-slide" src="<c:url value='../resources/images/background/back1.jpg'/>"
                     alt="First slide">
                <div class="container">
                    <div class="carousel-caption text-left">
                        <h1>Create an Account</h1>
                        <p>Create an Account at Nurstore to get benefits and more.</p>
                        <p><a class="btn btn-lg btn-primary" href="<c:url value="/register"/>" role="button">Sign up
                            today</a></p>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <img class="first-slide" src="<c:url value='../resources/images/background/back2.jpg'/>"
                     alt="Second slide">
                <div class="container">
                    <div class="carousel-caption">
                        <h1>See all our great products</h1>
                        <p>We have different sport nutrition types for men, women and teenagers. Go find yours!</p>
                        <p><a class="btn btn-lg btn-primary" href="<c:url value="/product/productList/all"/>"
                              role="button">Browse Gallery</a></p>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <img class="first-slide" src="<c:url value='../resources/images/background/back3.jpg'/>"
                     alt="Third slide">
                <div class="container">
                    <div class="carousel-caption text-right">
                        <h1>Learn more about Nurstore</h1>
                        <p>Do you want to get an additional information about our store? We are here to provide it for
                            you</p>
                        <p><a class="btn btn-lg btn-primary" href="/about" role="button">Learn more</a></p>
                    </div>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>


    <!-- Marketing messaging and features
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing">

        <div class="row">
            <div class="col-lg-4">
                <a class="btn btn-primary"
                   href="<c:url value="/product/productList?searchCondition=men"/>"
                   role="button">
                    <img class="img-circle" src="<c:url value="/resources/icons/men.jpg"/>"
                         alt="Men Nutrition" width="140" height="140">
                </a>
                <h2>Men Nutrition</h2>
                <p>High quality sport nutrition for men</p>

            </div>


            <div class="col-lg-4">
                <a class="btn btn-primary"
                   href="<c:url value="/product/productList?searchCondition=women"/>"
                   role="button">
                    <img class="img-circle" src="<c:url value="/resources/icons/women.jpg"/>"
                         alt="Women Nutrition" width="140" height="140">
                </a>
                <h2>Women Nutrition</h2>
                <p>High quality sport nutrition for women</p>

            </div>


            <div class="col-lg-4">
                <a class="btn btn-primary"
                   href="<c:url value="/product/productList?searchCondition=teenagers"/>"
                   role="button">
                    <img class="img-circle" src="<c:url value="/resources/icons/teenagers.jpg"/>"
                         alt="Teenagers Nutrition" width="140" height="140">
                </a>
                <h2>Teenagers Nutrition</h2>
                <p>High quality sport nutrition for teenagers</p>

            </div>

        </div>

    </div><!-- /.container -->

<%@ include file="templates/footer.jsp" %>