<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ include file="templates/header.jsp" %>
<div class="container-wrapper">
    <div class="container">
        <section>
            <div class="jumbotron">
                <div class="container">
                    <h1>Customer registered successfully!</h1>
                </div>
            </div>
        </section>

        <section class="container">
            <p>
                <a href="<spring:url value="/product/productList" />" class="btn btn-lg btn-primary">Products</a>
            </p>
        </section>

    </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/cartController.js"></script>

<%@ include file="templates/footer.jsp" %>
