<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
          integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

    <!-- Carousel CSS -->
    <link href="<c:url value="/resources/css/carousel.css" />" rel="stylesheet">

    <!-- Main CSS -->
    <link href="<c:url value="/resources/css/main.css" />" rel="stylesheet">

    <link href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" rel="stylesheet">


    <!-- Angular JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

    <script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
            integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
            crossorigin="anonymous"></script>

    <link rel="icon" href="${pageContext.request.contextPath}/docs/4.0/assets/img/favicons/favicon.ico">

    <title>Welcome to Nurstore Store</title>

</head>

<body>

<header>
    <div class="navbar-wrapper">
        <div class="container">
            <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
                <a class="nav-link" href="<c:url value="/"/>">Nurstore</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                        aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="navbar-brand" href="<c:url value="/product/productList/all"/>">Products <span
                                    class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/about">About Us<span
                                    class="sr-only">(current)</span></a>
                        </li>
                    </ul>

                    <ul class="nav navbar-nav pull-right ml-auto">
                        <c:if test="${pageContext.request.userPrincipal.name != null}">

                            <li class="nav-item"><a
                                    class="navbar-brand">Welcome: ${pageContext.request.userPrincipal.name}<span
                                    class="sr-only">(current)</span></a>
                            </li>

                            <li class="nav-item"><a class="navbar-brand" href="<c:url value="/logout"/>">Logout<span
                                    class="sr-only">(current)</span></a>
                            </li>

                            <c:if test="${pageContext.request.userPrincipal.name != 'admin'}">

                                <li class="nav-item"><a class="navbar-brand" href="<c:url value="/customer/cart"/> ">Cart<span
                                        class="sr-only">(current)</span></a>
                                </li>
                            </c:if>
                            <c:if test="${pageContext.request.userPrincipal.name == 'admin'}">
                                <li class="nav-item"><a class="navbar-brand" href="<c:url value="/admin"/>"> Admin <span
                                        class="sr-only">(current)</span></a>
                                </li>
                            </c:if>
                        </c:if>
                        <c:if test="${pageContext.request.userPrincipal.name == null}">
                            <li class="nav-item"><a class="navbar-brand" href="<c:url value="/login"/>"> Login <span
                                    class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item"><a class="navbar-brand" href="<c:url value="/register"/>"> Register
                                <span class="sr-only">(current)</span></a>
                            </li>
                        </c:if>

                    </ul>
            </nav>
        </div>
    </div>
</header>
